package br.edu.ifpb.pweb2.banco.dao;

import java.io.Serializable;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.edu.ifpb.pweb2.banco.business.model.Correntista;

@Repository
public class CorrentistaDAO extends GenericDAO<Correntista, Integer> implements Serializable {
	private static final long serialVersionUID = 1L;

	public Correntista findByLogin(String login) {
		Correntista c = null;
		try {
			Query q = this.entityManager.createQuery("from Correntista t where t.email = :login");
			q.setParameter("login", login);
			c = (Correntista) q.getSingleResult();
		} catch (NoResultException e) {
			c = null;
		}
		return c;
	}
	
	public void deleteAll() {
		Query q = this.entityManager.createNativeQuery("delete from tb_correntista");
		q.executeUpdate();
	}

}
