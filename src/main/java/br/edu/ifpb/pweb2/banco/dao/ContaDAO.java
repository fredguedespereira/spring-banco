package br.edu.ifpb.pweb2.banco.dao;

import java.io.Serializable;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.edu.ifpb.pweb2.banco.business.model.Conta;
import br.edu.ifpb.pweb2.banco.business.model.Correntista;

@Repository
public class ContaDAO extends GenericDAO<Conta, Integer> implements Serializable {
	private static final long serialVersionUID = 1L;

	public Conta findByIdWithTransacoes(Integer id) {
		try {
			Query q = this.entityManager
					.createQuery("select distinct c from Conta c left join fetch c.transacoes t where c.id = :id");
			q.setParameter("id", id);
			return (Conta) q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}

	}

	public Conta findByNumeroWithTransacoes(String numero) {
		try {
			Query q = this.entityManager
					.createQuery("from Conta c left join fetch c.transacoes t where c.numero = :numero");
			q.setParameter("numero", numero);
			return (Conta) q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}

	}

	public Conta findByCorrentista(Correntista correntista) {
		try {
			Query q = this.entityManager
					.createQuery("from Conta c left join fetch c.transacoes t where c.correntista = :correntista");
			q.setParameter("correntista", correntista);
			return (Conta) q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

}
