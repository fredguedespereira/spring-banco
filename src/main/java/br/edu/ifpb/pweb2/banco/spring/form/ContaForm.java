package br.edu.ifpb.pweb2.banco.spring.form;

import br.edu.ifpb.pweb2.banco.business.model.Conta;

public class ContaForm {
	private Conta conta;
	private Integer correntistaId;

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public Integer getCorrentistaId() {
		return correntistaId;
	}

	public void setCorrentistaId(Integer correntistaId) {
		this.correntistaId = correntistaId;
	}

}
