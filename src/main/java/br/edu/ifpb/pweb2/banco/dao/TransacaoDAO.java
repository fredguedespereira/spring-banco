package br.edu.ifpb.pweb2.banco.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.edu.ifpb.pweb2.banco.business.model.Conta;
import br.edu.ifpb.pweb2.banco.business.model.Transacao;

@Repository
public class TransacaoDAO extends GenericDAO<Transacao, Integer> implements Serializable {
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	public List<Transacao> getTransacoes(Conta conta) {
		Query q = this.entityManager.createQuery("from Transacao t where t.conta = :conta");
		q.setParameter("conta", conta);
		return (List<Transacao>) q.getResultList();
	}

}
