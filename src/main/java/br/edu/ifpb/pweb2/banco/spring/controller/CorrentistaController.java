package br.edu.ifpb.pweb2.banco.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.ifpb.pweb2.banco.business.model.Correntista;
import br.edu.ifpb.pweb2.banco.business.service.CorrentistaService;

@Controller
@RequestMapping("/correntistas")
public class CorrentistaController {
	
	@Autowired
	private CorrentistaService correntistaService;
	
	@RequestMapping("/insert")
	public ModelAndView insertBD(ModelAndView modelAndView) {
		correntistaService.insertBD();
		modelAndView.setViewName("correntistas/list");
		modelAndView.addObject("correntistas", correntistaService.findAll());
		return modelAndView;
	}


}
