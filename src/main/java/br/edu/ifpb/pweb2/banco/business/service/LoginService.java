package br.edu.ifpb.pweb2.banco.business.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.ifpb.pweb2.banco.business.model.Correntista;
import br.edu.ifpb.pweb2.banco.dao.CorrentistaDAO;
import br.edu.ifpb.pweb2.banco.util.PasswordUtil;

@Service
public class LoginService {
	
	@Autowired
	private CorrentistaDAO correntistaDAO;

	public Correntista isValido(Correntista correntista) {
		Correntista correntistaBD = correntistaDAO.findByLogin(correntista.getEmail());
		boolean valido = false;
		if (correntistaBD != null) {
			if (PasswordUtil.checkPass(correntista.getSenha(), correntistaBD.getSenha())) {
				valido = true;
			}
		} 
		return valido ? correntistaBD : null;
	}
	

}
