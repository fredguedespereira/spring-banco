package br.edu.ifpb.pweb2.banco.business.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.ifpb.pweb2.banco.business.exception.BancoException;
import br.edu.ifpb.pweb2.banco.business.model.Conta;
import br.edu.ifpb.pweb2.banco.business.model.Correntista;
import br.edu.ifpb.pweb2.banco.business.model.Transacao;
import br.edu.ifpb.pweb2.banco.dao.ContaDAO;

@Service
public class ContaService {
	
	@Autowired
	private ContaDAO contaDAO;

	@Transactional
	public void saveConta(Conta conta) {
		if (conta.getId() == null) {
			contaDAO.save(conta);
		} else {
			contaDAO.update(conta);
		}
		
	}
	
	public Conta findById(Integer id) {
		return contaDAO.findById(id);
	}
	
	public Conta findByCorrentista(Correntista correntista) {
		return contaDAO.findByCorrentista(correntista);
	}
	
	public Conta findByNumeroWithTransacoes(String numero) {
		return contaDAO.findByNumeroWithTransacoes(numero);
	}

	public Conta findByIdWithTransacoes(Integer id) {
		return contaDAO.findByIdWithTransacoes(id);
	}

	public List<Conta> findAll() throws BancoException {
		return contaDAO.findAll();
	}

	@Transactional
	public void deleteById(Integer id) {
		contaDAO.deleteById(id);
	}

	@Transactional
	public void addTransacao(Conta conta, Transacao transacao) {
		conta.addTransacao(transacao);
		contaDAO.update(conta);
		
	}

}
